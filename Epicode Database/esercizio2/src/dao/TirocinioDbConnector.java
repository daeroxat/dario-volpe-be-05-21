package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import util.ConnectionHandler;

public class TirocinioDbConnector implements TirocinanteDao {

	@Override
	public List<Object[]> cercaTirocinanti() throws ClassNotFoundException, SQLException {

		Connection connessione = null;
		PreparedStatement query = null;
		ResultSet risultati = null;
		List<Object[]> listaTirocinanti = new ArrayList<Object[]>();
		ConnectionHandler connHand = new ConnectionHandler("Aziende", "postgres", "73237517");

		try {

			connessione = connHand.getConnection();

			query = connHand.getPreparedStatement("SELECT id, nome, cognome, classe FROM Tirocinante");
			risultati = query.executeQuery();

			// scorro i risultati per visualizzarli
			while (risultati.next()) {
				Object[] obj = new Object[4];
				obj[0] = risultati.getInt(1);
				obj[1] = risultati.getString(2);
				obj[2] = risultati.getString(3);
				obj[3] = risultati.getString(4);

				listaTirocinanti.add(obj);
			}

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			connHand.closeConnection();
		}
		return listaTirocinanti;
	}

	@Override
	public List<Object[]> cercaTutor(Connection connection) throws SQLException {
		Connection connessione = connection;
		Statement query = null;
		ResultSet risultati = null;
		List<Object[]> listaTutor = new ArrayList<Object[]>();

		try {

			query = connessione.createStatement();
			risultati = query.executeQuery("SELECT id, nome, cognome, materia FROM Tutor");

			while (risultati.next()) {
				Object[] obj = new Object[4];
				obj[0] = risultati.getInt(1);
				obj[1] = risultati.getString(2);
				obj[2] = risultati.getString(3);
				obj[3] = risultati.getString(4);

				listaTutor.add(obj);
			}

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			// chiusura connessione
			connessione.close();
		}
		return listaTutor;
	}

	@Override
	public void insertTirocinante(String matricola, String nome, String cognome, String classe, long idazienda,
			long idtutor) throws ClassNotFoundException, SQLException {

		Connection connessione = null;
		PreparedStatement query = null;
		ConnectionHandler connHand = new ConnectionHandler("Aziende", "postgres", "73237517");

		try {

			connessione = connHand.getConnection();

			query = connHand.getPreparedStatement(
					"INSERT INTO Tirocinante(matricola, nome, cognome, classe, id_azienda, id_tutor) VALUES (?, ?, ?, ?, ?, ?)");
			query.setString(1, matricola);
			query.setString(2, nome);
			query.setString(3, cognome);
			query.setString(4, classe);
			query.setLong(5, idazienda);
			query.setLong(6, idtutor);
			query.executeUpdate();

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			// chiusura connessione
			connHand.closeConnection();
		}

	}

	@Override
	public void updateTirocinante(long id, String matricola, String nome, String cognome, String classe, long idazienda,
			long idtutor) throws ClassNotFoundException, SQLException {
		Connection connessione = null;
		PreparedStatement query = null;
		ConnectionHandler connHand = new ConnectionHandler("Aziende", "postgres", "73237517");

		try {
			connessione = connHand.getConnection();

			// esecuzione update
			query = connHand.getPreparedStatement("UPDATE Tirocinante\n"
					+ "SET matricola = ?, nome = ?, cognome = ?, classe = ?, id_azienda = ?, id_tutor = ?\n"
					+ "WHERE id = ?;");
			query.setLong(7, id);
			query.setString(1, matricola);
			query.setString(2, nome);
			query.setString(3, cognome);
			query.setString(4, classe);
			query.setLong(5, idazienda);
			query.setLong(6, idtutor);
			query.executeUpdate();

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			// chiusura connessione
			connHand.closeConnection();
		}

	}

	@Override
	public void deleteTirocinante(long id) throws SQLException, ClassNotFoundException {

		Connection connessione = null;
		PreparedStatement query = null;
		ConnectionHandler connHand = new ConnectionHandler("Aziende", "postgres", "73237517");

		try {

			// creazione connessione
			connessione = connHand.getConnection();

			// esecuzione delete
			query = connessione.prepareStatement("DELETE FROM Tirocinante WHERE id = ?");
			query.setLong(1, id);
			query.executeUpdate();

			// chiusura connessione

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			connHand.closeConnection();
		}

	}

	@Override
	public List<Object[]> cercaTirocinantiPerAzienda(String nomeAzienda) throws SQLException, ClassNotFoundException {

		Connection connessione = null;
		PreparedStatement query = null;
		ResultSet risultati = null;
		List<Object[]> listaTirocinanti = new ArrayList<Object[]>();
		ConnectionHandler connHand = new ConnectionHandler("Aziende", "postgres", "73237517");

		try {

			// creazione connessione
			connessione = connHand.getConnection();

			// creazione statement di estrazione
			query = connHand.getPreparedStatement(
					"SELECT t.id, t.nome, t.cognome, t.classe \n" + "FROM Tirocinante t, Azienda a \n"
							+ "WHERE a.id = t.id_azienda AND a.nome = '" + nomeAzienda + "';");
			risultati = query.executeQuery();

			// scorro i risultati per visualizzarli
			while (risultati.next()) {
				Object[] obj = new Object[4];
				obj[0] = risultati.getInt(1);
				obj[1] = risultati.getString(2);
				obj[2] = risultati.getString(3);
				obj[3] = risultati.getString(4);

				listaTirocinanti.add(obj);
			}

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		} finally {
			// chiusura connessione
			connHand.closeConnection();
		}
		return listaTirocinanti;
	}

}
