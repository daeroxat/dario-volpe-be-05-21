package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TirocinanteDao {
	List<Object[]> cercaTirocinanti() throws ClassNotFoundException, SQLException;
	List<Object[]> cercaTutor(Connection connection) throws ClassNotFoundException, SQLException;
	void insertTirocinante(String matricola,String nome, String cognome,String classe, long idazienda, long idtutor ) throws ClassNotFoundException, SQLException;
	void updateTirocinante(long id, String matricola,String nome, String cognome,String classe, long idazienda, long idtutor ) throws ClassNotFoundException, SQLException;
	void deleteTirocinante(long id) throws SQLException, ClassNotFoundException;
	List<Object[]> cercaTirocinantiPerAzienda(String nomeAzienda) throws SQLException, ClassNotFoundException;
}
