package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionHandler {

	private Connection connection;
	private String db_url;
	private String username;
	private String password;
	

	public ConnectionHandler(String dB_URL, String uSERNAME, String pASSWORD) throws ClassNotFoundException {
		
		db_url = dB_URL;
		username = uSERNAME;
		password = pASSWORD;
		//Class.forName("org.postgresql.Driver");
	}

	

	public Connection getConnection() throws SQLException {
		String Conn_url = "jdbc:postgresql://localhost:5432/" + db_url;
		if (connection == null || connection.isClosed()) {
			connection = DriverManager.getConnection(Conn_url, username, password);
		}
		return connection;

	}

	public void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
			connection = null;
		}
	}

	public PreparedStatement getPreparedStatement(String query) {
		try {

			connection =  getConnection();
			PreparedStatement pstmt = connection.prepareStatement(query);
			return pstmt;
		} catch (Exception e) {
			return null;
		}
	}

}
