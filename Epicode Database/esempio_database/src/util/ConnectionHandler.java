package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionHandler {

	private Connection connection;
	private String DB_URL;
	private String SCHEMA;
	private String USERNAME;
	private String PASSWORD;
	

	public ConnectionHandler(String dB_URL, String sCHEMA, String uSERNAME, String pASSWORD) throws ClassNotFoundException {
		DB_URL = dB_URL;
		SCHEMA = sCHEMA;
		USERNAME = uSERNAME;
		PASSWORD = pASSWORD;
	}

	

	public Connection getConnection() throws SQLException {
		String Conn_url = "jdbc:postgresql://localhost:5432/" + DB_URL;
		if (connection == null || connection.isClosed()) {
			connection = DriverManager.getConnection(Conn_url, USERNAME, PASSWORD);
		}
		return connection;

	}

	public void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
			connection = null;
		}
	}

	public PreparedStatement getPreparedStatement(String query) {
		try {

			connection =  getConnection();
			PreparedStatement pstmt = connection.prepareStatement(query);
			return pstmt;
		} catch (Exception e) {
			return null;
		}
	}

}
