package dao;

public class AziendaDao {
	
	private int id;
	private int id_azienda = 1;
	private String nome;
	private String luogo;
	private String settore;
	private String tipologia;

	public AziendaDao(String nome, String luogo, String settore, String tipologia) {
		this.id = id_azienda;
		this.nome = nome;
		this.luogo = luogo;
		this.settore = settore;
		this.tipologia = tipologia;
		id_azienda++;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLuogo() {
		return luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

}
