package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.*;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		ConnectionHandler conn = new ConnectionHandler("Aziende", "Tirocinanti", "postgres", "73237517");
		PreparedStatement statement = conn.getPreparedStatement("SELECT * FROM Azienda");

		Connection connessione = null;
		Statement query = null;
		ResultSet risultati = null;

		try {

			connessione = conn.getConnection();
			query = connessione.createStatement();
			risultati = query.executeQuery("SELECT * FROM Azienda");

			
			while (risultati.next()) {
				System.out.println(risultati.getString("nome") + ", " + risultati.getString("luogo") + ", "
						+ risultati.getString("settore") + ", " + risultati.getString("settore") + ", "
						+ risultati.getString("tipologia"));
				
			}

			
			connessione.close();

		} catch (SQLException ex) {
			System.out.println("errore: " + ex.getMessage());
		}
	}

}
