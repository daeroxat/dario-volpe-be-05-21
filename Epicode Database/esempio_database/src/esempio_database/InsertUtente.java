package jdbcNoMavenExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUtente {

    public void inserisciUtente(String username, String password) {

    	Connection connessione = null;
    	Statement query = null;

    	//driver postgres
    	try {        	
        	Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        	System.out.println("Driver non trovato");
        } 
    	
        try {
        	
        	//creazione connessione
        	connessione = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Utenti", "postgres", "postgres");
        
        	//esecuzione update
        	query = connessione.createStatement();
        	query.executeUpdate("INSERT INTO utente(username, password) VALUES ('" + username + "','" + password + "')"); 

        	//chiusura connessione
        	connessione.close();
        	
        } catch (SQLException ex) {
        	System.out.println("errore: " + ex.getMessage());
        }
    }
    
}
