package esempio_database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DeleteUtente {

    public void deleteUtente(String name) {

    	Connection connessione = null;
    	Statement query = null;
        
    	//driver postgres
    	try {        	
        	Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        	System.out.println("Driver non trovato");
        }        	
        	
        try {
        	
        	//creazione connessione
        	connessione = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Aziende", "postgres", "73237517");
        
        	//esecuzione delete
        	query = connessione.createStatement();
        	query.executeUpdate("DELETE FROM Tirocinante WHERE nome = '" + name + "'"); 

        	//chiusura connessione
        	connessione.close();
        	
        } catch (SQLException ex) {
        	System.out.println("errore: " + ex.getMessage());
        }
    }
    
    public void deleteUtentePrepared(String nome) {

    	Connection connessione;
    	PreparedStatement query;

    	//driver postgres
    	try {        	
        	Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        	System.out.println("Driver non trovato");
        } 
    	
        try {
        	
        	//creazione connessione
        	connessione = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Utenti", "postgres", "postgres");
        
        	//esecuzione delete
        	query = connessione.prepareStatement("DELETE FROM utente WHERE username = ?");
        	query.setString(1, nome); 
        	query.executeUpdate();

        	//chiusura connessione
        	connessione.close();
        	
        } catch (SQLException ex) {
        	System.out.println("errore: " + ex.getMessage());
        }
    }
    
}
