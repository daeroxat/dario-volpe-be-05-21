package jdbcNoMavenExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateUtente {

    public void updateUtente(String username, String nuovaPassword) {

    	Connection connessione = null;
    	Statement query = null;

    	//driver postgres
    	try {        	
        	Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        	System.out.println("Driver non trovato");
        } 
    	
        try {
        	
        	//creazione connessione
        	connessione = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Utenti", "postgres", "postgres");
        
        	//esecuzione update
        	query = connessione.createStatement();
        	query.executeUpdate("UPDATE utente SET password = '"+ nuovaPassword + "' WHERE username = '" + username + "'"); 

        	//chiusura connessione
        	connessione.close();
        	
        } catch (SQLException ex) {
        	System.out.println("errore: " + ex.getMessage());
        }
    }
    
}
