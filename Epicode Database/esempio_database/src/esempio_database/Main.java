package esempio_database;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("utenti iniziale");
		
		//leggo tutti gli utenti
		GetUtenti getUtenti = new GetUtenti();
		getUtenti.leggiUtenti();
		
		/*
		 * System.out.println("--------------------");
		 * System.out.println("utenti dopo inserimento pippo");
		 * 
		 * //inserisco un nuovo utente (pippo) InsertUtente insertUtente = new
		 * InsertUtente(); insertUtente.inserisciUtente("pippo", "pluto");
		 * 
		 * getUtenti.leggiUtenti();
		 * 
		 * System.out.println("--------------------");
		 * System.out.println("utenti dopo eliminazione pippo");
		 * 
		 * //elimino pippo DeleteUtente deleteUtente = new DeleteUtente();
		 * deleteUtente.deleteUtente("pippo");
		 * 
		 * getUtenti.leggiUtenti();
		 * 
		 * System.out.println("--------------------");
		 * System.out.println("utenti dopo modifica password pippo");
		 * 
		 * //modifico la password di pippo UpdateUtente updateUtente = new
		 * UpdateUtente(); insertUtente.inserisciUtente("pippo", "pluto");
		 * updateUtente.updateUtente("pippo", "nuovapassword");
		 * 
		 * getUtenti.leggiUtenti();
		 * 
		 * System.out.println("--------------------");
		 * System.out.println("utenti dopo eliminazione pippo (prepared statement)");
		 * 
		 * //elimino pippo (prepared statement)
		 * deleteUtente.deleteUtentePrepared("pippo");
		 */	
		//getUtenti.leggiUtenti();		
		
	}

}
